public class Solution
{
    public double FindMedianSortedArrays(int[] nums1, int[] nums2)
    {
        int[] combined = nums1.Concat(nums2).ToArray();
        Array.Sort(combined);
        if (combined.Length % 2 == 1)
            return combined[combined.Length / 2];
        return (double)(combined[(combined.Length - 1) / 2] + combined[((combined.Length - 1) / 2) + 1]) / 2;
    }
}
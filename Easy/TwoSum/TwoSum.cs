public class Solution
{
    public int[] TwoSum(int[] nums, int target)
    {
        Dictionary<int, int> dictionary = new Dictionary<int, int>();

        for (int i = 0; i < nums.Length; i++)
            if (dictionary.ContainsKey((target - nums[i])))
                return new[] { dictionary[(target - nums[i])], i };
            else
                dictionary.TryAdd(nums[i], i);
        return null;
    }
}
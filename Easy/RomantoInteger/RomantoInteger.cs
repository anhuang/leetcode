public class Solution
{
    public int RomanToInt(string s)
    {
        int ret = 0;
        if (s == null || s.Length == 0) return 0;

        int previousValue = -1;
        int result = 0;
        Dictionary<char, int> d = new Dictionary<char, int>();

        d.Add('I', 1);
        d.Add('V', 5);
        d.Add('X', 10);
        d.Add('L', 50);
        d.Add('C', 100);
        d.Add('D', 500);
        d.Add('M', 1000);

        for (int i = s.Length - 1; i >= 0; i--)
        {
            int currentValue = d[s[i]];

            if (currentValue < previousValue)
            {
                currentValue = currentValue * -1;
            }

            previousValue = d[s[i]];
            result += currentValue;
        }

        return result;
    }
}
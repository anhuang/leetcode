/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     public int val;
 *     public ListNode next;
 *     public ListNode(int x) { val = x; }
 * }
 */
public class Solution
{
    public ListNode AddTwoNumbers(ListNode l1, ListNode l2)
    {
        int sum = 0;
        ListNode ret = new ListNode(0);
        ListNode cur = ret;
        while (l1 != null && l2 != null)
        {
            sum += l1.val + l2.val;
            cur.next = new ListNode(sum % 10);
            sum /= 10;
            cur = cur.next;
            l1 = l1.next;
            l2 = l2.next;
        }
        if (l1 == null && l2 == null)
        {
            if (sum > 0)
                cur.next = new ListNode(sum % 10);
            return ret.next;
        }
        if (l1 == null)
            l1 = l2;
        while (l1 != null)
        {
            sum += l1.val;
            cur.next = new ListNode(sum % 10);
            sum /= 10;
            cur = cur.next;
            l1 = l1.next;
        }
        if (sum > 0)
            cur.next = new ListNode(sum % 10);
        return ret.next;
    }
}
public class Solution
{
    public int MaxArea(int[] height)
    {
        //       int ret = 0;      //First Algo Slow
        //       for(int start = 0; start < height.Length; start++)
        //       {
        //           for(int end = start + 1; end < height.Length; end++)
        //           {
        //              if ((end - start) * (height[end] < height[start] ? height[end] : height[start]) > ret)
        //                   ret = (end - start) * (height[end] < height[start] ? height[end] : height[start]);
        //           }
        //       }
        //       return ret;
        int maxArea = 0, start = 0, end = height.Length - 1; //Second Faster
        while (start < end)
        {
            if (height[start] < height[end])
            {
                int newArea = (end - start) * height[start];
                start++;
                if (newArea > maxArea)
                {
                    maxArea = newArea;
                }
            }
            else
            {
                int newArea = (end - start) * height[end];
                end--;
                if (newArea > maxArea)
                {
                    maxArea = newArea;
                }
            }
        }
        return maxArea;
    }
}
public class Solution
{
    public IList<IList<int>> ThreeSum(int[] nums)
    {
        IList<IList<int>> bigTab = new List<IList<int>>();

        Array.Sort(nums);
        for (int i = 0; i < nums.Length; i++)
        {
            if (i > 0 && nums[i] == nums[i - 1])
                continue;

            int startIndex = i + 1;
            int endIndex = nums.Length - 1;
            int targetSum = -(nums[i]);
            while (startIndex < endIndex)
            {
                if (nums[startIndex] + nums[endIndex] == targetSum)
                {
                    bigTab.Add(new int[] { nums[i], nums[startIndex], nums[endIndex] });
                    while (startIndex < endIndex && nums[startIndex] == nums[startIndex + 1])
                        startIndex++;
                    while (startIndex < endIndex && nums[endIndex] == nums[endIndex - 1])
                        endIndex--;
                    startIndex++;
                    endIndex--;
                }
                else
                {
                    if (nums[startIndex] + nums[endIndex] < targetSum)
                        startIndex++;
                    else
                        endIndex--;
                }
            }
        }
        return bigTab;
    }
}
class Solution {
public:
    vector<vector<int>> fourSum(vector<int>& nums, int target) {
        vector<vector<int>> ret;
        sort(nums.begin(), nums.end());
        if (nums.size() < 4)
            return ret;
        for (int y = 0; y < nums.size() - 3; y++) {
            for (int x = y + 1; x < nums.size() - 2; x++) {
                for (int a = x + 1; a < nums.size() - 1; a++) {
                    for (int i = a + 1; i < nums.size(); i++) {
                        if (nums[y] + nums[x] + nums[a] + nums[i] == target) {
                            vector<int> vc{nums[y], nums[x], nums[a], nums[i]};
                            if (std::binary_search(ret.begin(), ret.end(), vc))
                               continue;
                            ret.push_back(vc);
                        }
                    }
                }
            }
        }
        return ret;
    }
};

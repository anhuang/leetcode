public class Solution
{
    public int LengthOfLongestSubstring(string s)
    {
        int max = 0;
        int start = 0;
        for (int end = 1; end <= s.Length; end++)
        {
            string sub = s.Substring(start, end - start);
            if (sub.IndexOf(sub[sub.Length - 1]) != sub.Length - 1 && sub.Length != 1) // if get occurence with the new letter
                start += sub.IndexOf(sub[sub.Length - 1]) + 1; // get occurence + 1 letter
            max = Math.Max(end - start, max); // get max value between latest word.Length and new the one
        }
        return max;
    }
}
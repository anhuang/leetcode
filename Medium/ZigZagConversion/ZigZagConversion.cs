public class Solution
{
    public string Convert(string s, int numRows)
    {
        string ret = new StringBuilder(s.Length);
        int a = 0;         // Snake Variable
        int curRow = 0;
        int curLet = 0;
        bool up = false;
        if (numRows < 2)
            return s;
        for (int i = 0; i < s.Length && curRow < numRows; i++)
        {
            if (curRow == a)
            {
                ret += s[i];
                curLet++;
            }
            a += up ? -1 : 1;
            if (a >= numRows - 1 || a == 0) // Changing direction of reading
                up = !up;
            if (i == s.Length - 1)
            {
                curRow++;
                i = 0;
                a = 1;
                up = false;
            }
        }
        return ret;
    }
}
/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    ListNode* Renode(ListNode *node) {
        ListNode *tmp = node->next;
        
        node->next = node->next->next;
        delete tmp;
        return node;
    }
    
    ListNode* removeNthFromEnd(ListNode* head, int n) {
        if (n == 1 && !head->next)
            return NULL;
        ListNode *baseNode = head;
        if (n == 1) {
            while (baseNode->next->next)
                baseNode = baseNode->next;
            delete baseNode->next->next;
            baseNode->next = NULL;
            return head;
        }
        if (n == 2 && !head->next->next)
            return head->next;
        int index = 0;
        while (true) {
            ListNode *tmp = baseNode;
            for (int i = n - 2; i > 0; i--)
                tmp = tmp->next;
            if (tmp->next->next == NULL) {
                if (index == 0)
                    return head->next;
                tmp = head;
                for (int a = index - 1; a > 0; a--)
                    tmp = tmp->next;
                tmp = Renode(tmp);
                return head;
            }
            baseNode = baseNode->next;
            index++;
        }
        return head;
    }
};
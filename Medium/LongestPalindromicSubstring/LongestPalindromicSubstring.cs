public class Solution
{
    private int startIndex;
    private int length;

    public string LongestPalindrome(string s)
    {
        for (int i = 0; i < s.Length; i++)
        {
            //For odd length.
            this.ExtendPallindrome(s, i, i);

            //For even length.
            this.ExtendPallindrome(s, i, i + 1);
        }

        return s.Substring(this.startIndex, this.length);
    }

    private void ExtendPallindrome(string s, int start, int end)
    {
        while (start >= 0 && end < s.Length && s[start] == s[end])
        {
            start--;
            end++;
        }
        int newLength = ((end - 1) - (start + 1)) + 1;

        if (this.length < newLength)
        {
            this.startIndex = start + 1;
            this.length = newLength;
        }
    }
}
public class Solution {
    public int ThreeSumClosest(int[] nums, int target) {
        int closestNum = 0;
        bool firstTime = true;
        Array.Sort(nums);
        for (int i = 0; i < nums.Length; i++)
        {
            int startIndex = i + 1;
            int endIndex = nums.Length - 1;
            int targetSum = target - nums[i];
            while (startIndex < endIndex)
            {
                if (nums[startIndex] + nums[endIndex] == targetSum)
                    return target;
                else
                {
                    if (firstTime || Math.Abs(targetSum - (nums[startIndex] + nums[endIndex])) < Math.Abs(target - closestNum))
                    {
                        closestNum = nums[i] + nums[startIndex] + nums[endIndex];
                        firstTime = false;
                    }
                    if (nums[startIndex] + nums[endIndex] < targetSum)
                        startIndex++;
                    else
                        endIndex--;
                }
            }
        }
        return closestNum;
    }
}
